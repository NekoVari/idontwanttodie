#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

int main() {
    int table[8][8]={{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0}};
    int n,x,y,out=0;
    scanf("%d",&n);
    for(int i=0;i<n;i++){
        scanf("%d %d",&x,&y);
        for(int j=0;j<8;j++){
            table[j][y-1]++;
        }
        for(int j=0;j<8;j++){
            table[x-1][j]++;
        }
    }
    for(int i=0;i<8;i++){
       for(int j=0;j<8;j++){
            if(table[i][j]==0)out++;
        }
    }
    printf("%d",out);
}
